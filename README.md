# NgCrypto
[![](https://img.shields.io/badge/angular-7.0.3-blue.svg)](https://angular.io)
[![](https://img.shields.io/badge/angular cli-7.0.3-blue.svg)](https://cli.angular.io)
[![](https://img.shields.io/badge/typescript-3.1.3-blue.svg)](https://www.typescriptlang.org/)
[![](https://img.shields.io/badge/npm-6.4.1-blue.svg)](https://www.npmjs.com)
[![Build Status](https://semaphoreci.com/api/v1/projects/a77fec50-a110-4c63-b312-6903083475e0/2318338/shields_badge.svg)](https://semaphoreci.com/nrozic/ng-crypto)



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `npm install` and `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing on mobile device

To view application on mobile device, run `ng serve` with host flag where host value is IP addres of your computer. Example: `ng serve --host 192.168.1.1` and on mobile device open up that IP addres on port 4200. Please make sure that your mobile device is connected on the same network as computer

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
