export const environment = {
    production: true,
    routeTracking: false,
    appTitle: 'CoinExchange',
    coinmarketBaseUrl: 'https://api.coinmarketcap.com/v2/',
};
