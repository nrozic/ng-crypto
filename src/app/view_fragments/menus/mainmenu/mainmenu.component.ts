import { Component, OnInit } from '@angular/core';
import { _STRINGS, _ROUTES } from 'src/app/commons/constants/app-constants';

export interface IMenu {
    title: string;
    route: string;
}

const MENU: IMenu[] = [
    { title: 'Home', route: `${_ROUTES.home}` },
    { title: 'Currencies', route: `/${_ROUTES.currencies}` },
    { title: 'Wallet', route: `/${_ROUTES.wallet}` },
    { title: 'Not found', route: `/${_ROUTES.notFound}` },
];

export type IMenuIcon = 'menu' | 'close';

@Component({
    selector: 'app-mainmenu',
    templateUrl: './mainmenu.component.html',
    styleUrls: ['./mainmenu.component.scss']
})

export class MainmenuComponent implements OnInit {
    appName = _STRINGS.appName;
    homeRoute = _ROUTES.home;
    menu: IMenu[] = MENU;
    menuIcon: IMenuIcon = 'menu';

    constructor() { }

    ngOnInit() {
    }

    toggleMenu() {
        this.menuIcon = (this.menuIcon === 'menu') ? 'close' : 'menu';
    }

}
