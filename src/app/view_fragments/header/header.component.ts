import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { _STRINGS, _ROUTES } from 'src/app/commons/constants/app-constants';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    headerClass: boolean;
    isHome: boolean;
    pageTitle: string = _STRINGS.homepageTitle;
    tagline: string = _STRINGS.appTagline;

    constructor(
        private router: Router
    ) {
        this.addCssClassToHeader();
    }

    ngOnInit() {
    }

    ngOnDestroy() {

    }

    private addCssClassToHeader() {
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    const currentUrlSlug: string = event.url.slice(1);
                    this.headerClass = (currentUrlSlug) ? false : true;
                    this.isHome = (currentUrlSlug) ? false : true;
                }
            });
    }

}
