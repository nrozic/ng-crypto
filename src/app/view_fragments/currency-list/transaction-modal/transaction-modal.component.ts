import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

import { ModalService } from './modal.service';
import { Subscription } from 'rxjs';
import { ITicker } from 'src/app/commons/models/ticker.model';

@Component({
    selector: 'app-transaction-modal',
    templateUrl: './transaction-modal.component.html',
    styleUrls: ['./transaction-modal.component.scss']
})
export class TransactionModalComponent implements OnInit, OnDestroy {
    modalVisible: boolean;
    item: ITicker;
    amount = 0;
    total = 0;
    private subscriptions: Subscription = new Subscription();

    constructor(
        private modalService: ModalService
    ) { }

    ngOnInit() {
        const subscription = this.modalService.popupState.subscribe((value: boolean) => {
            this.modalVisible = value;

            if (this.modalVisible) {
                this.item = this.modalService.data;
            } else {
                this.item = null;
            }
        });

        this.subscriptions.add(subscription);
    }

    calculateTotalAmount(event: Event, amount: number, price: number) {
        this.total = parseFloat((amount * price).toFixed(2));
    }

    private clear() {
        this.amount = 0;
    }

    buy() {
        this.clear();
        this.modalService.close();
    }

    sell() {
        this.clear();
        this.modalService.close();
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }

}
