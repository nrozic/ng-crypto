import { Injectable, EventEmitter } from '@angular/core';
import { ITicker } from 'src/app/commons/models/ticker.model';

export interface IPopupData {
    title: string;
}

@Injectable({
    providedIn: 'root'
})
export class ModalService {
    data: ITicker;
    popupState = new EventEmitter();

    constructor() { }

    private open() {
        this.popupState.emit(true);
    }

    close() {
        this.data = null;
        this.popupState.emit(false);
    }

    openTransactionDialog(item: ITicker) {
        this.data = item;
        this.open();
    }
}
