import { Component, OnInit, Input, IterableDiffers, OnChanges } from '@angular/core';

import { ITicker } from 'src/app/commons/models/ticker.model';
import { _CONFIG, _STRINGS } from 'src/app/commons/constants/app-constants';
import { OrderByPipe } from 'src/app/commons/pipes/order-by.pipe';
import { ModalService } from 'src/app/view_fragments/currency-list/transaction-modal/modal.service';

export type ISortableFields = 'name' | 'price' | 'symbol' | 'marketCap' | 'percentChange24';

export interface IOrderBy {
    field: ISortableFields;
    asc: boolean;
    text: string;
}

const _ORDER_BY: IOrderBy[] = [
    { field: 'name', asc: true, text: 'Name (ASC)' },
    { field: 'name', asc: false, text: 'Name (DESC)' },
    { field: 'price', asc: true, text: 'Price (ASC)' },
    { field: 'price', asc: false, text: 'Price (DESC)' },
    { field: 'symbol', asc: true, text: 'Symbol (ASC)' },
    { field: 'symbol', asc: false, text: 'Symbol (DESC)' },
    { field: 'marketCap', asc: true, text: 'Market Cap (ASC)' },
    { field: 'marketCap', asc: false, text: 'Market Cap (DESC)' },
    { field: 'percentChange24', asc: true, text: 'Relative change 24h (ASC)' },
    { field: 'percentChange24', asc: false, text: 'Relative change 24h (DESC)' },
];

@Component({
    selector: 'app-currency-list',
    templateUrl: './currency-list.component.html',
    styleUrls: ['./currency-list.component.scss'],
    providers: [OrderByPipe]
})

export class CurrencyListComponent implements OnInit, OnChanges {
    @Input() list: ITicker[];
    filter: 'name';
    differ: any;
    orderBy: IOrderBy[] = _ORDER_BY;
    increasedPrices: Array<ITicker> = [];
    decreasedPrices: Array<ITicker> = [];
    marketCapLabel: string = _STRINGS.marketCapLabel;
    priceLabel: string = _STRINGS.priceLabel;

    constructor(
        private differs: IterableDiffers,
        private orderByPipe: OrderByPipe,
        private modalService: ModalService
    ) {
    }

    ngOnInit() {
        this.differ = this.differs.find([]).create(null);
    }

    applyFlter(event: Event) {
        const args = event.target['value'].split(',');
        const field = args[0];
        const asc = (args[1] === 'true') ? true : false;
        this.list = this.orderByPipe.transform(this.list, field, asc);
    }

    ngOnChanges(changes) {
        if (changes.list.previousValue && changes.list.previousValue.length > 0) {
            this.detectPriceChange(changes);
        }
    }

    priceChangedMark(id: number, subject: Array<ITicker>) {
        const match = subject.find(item => item.id === id);
        return match;
    }

    isChangePositive(value: number) {
        return (value > 0) ? true : false;
    }

    private detectPriceChange(changes) {
        let index = 0;

        this.increasedPrices = [];
        this.decreasedPrices = [];

        for (const item of changes.list.previousValue) {
            const oldPrice = changes.list.previousValue[index].price;
            const newPrice = changes.list.currentValue[index].price;
            const current: ITicker = changes.list.currentValue[index];

            if (oldPrice < newPrice) {
                this.increasedPrices.push(current);
            }

            if (oldPrice > newPrice) {
                this.decreasedPrices.push(current);
            }

            index++;
        }
    }

    openTransactionModal(event: MouseEvent, item: ITicker) {
        this.modalService.openTransactionDialog(item);
    }

}
