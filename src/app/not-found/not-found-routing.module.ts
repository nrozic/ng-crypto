import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { _ROUTES } from 'src/app/commons/constants/app-constants';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';


export const notFoundRoutes: Routes = [
    { path: '', component: NotFoundComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(notFoundRoutes),
    ],
    exports: [
        RouterModule
    ],
})
export class NotFoundRoutingModule { }
