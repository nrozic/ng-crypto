import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotFoundRoutingModule } from 'src/app/not-found/not-found-routing.module';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';



@NgModule({
    imports: [
        CommonModule,
        NotFoundRoutingModule,
    ],
    declarations: [
        NotFoundComponent
    ],
    exports: [
        NotFoundComponent
    ]
})
export class NotFoundModule { }
