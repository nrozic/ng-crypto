import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import { SharedModule } from 'src/app/commons/modules/shared.module';
import { HeaderComponent } from 'src/app/view_fragments/header/header.component';
import { MainmenuComponent } from 'src/app/view_fragments/menus/mainmenu/mainmenu.component';
import { FooterComponent } from 'src/app/view_fragments/footer/footer.component';
import { CurrenciesModule } from 'src/app/currencies/currencies.module';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MainmenuComponent,
        FooterComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CurrenciesModule,
        SharedModule.forRoot(),
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
