import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { _ROUTES } from 'src/app/commons/constants/app-constants';
import { environment } from 'src/environments/environment';

export const appRoutes: Routes = [
    { path: '', loadChildren: 'src/app/home/home.module#HomeModule' },
    { path: _ROUTES.currencies, loadChildren: 'src/app/currencies/currencies.module#CurrenciesModule' },
    { path: _ROUTES.notFound, loadChildren: 'src/app/not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: _ROUTES.notFound, pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { enableTracing: environment.routeTracking })
    ],
    exports: [
        RouterModule,
    ]
})
export class AppRoutingModule { }
