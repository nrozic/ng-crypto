export const _CONFIG = {
    updateFrequency: 10000,
};

export enum _ROUTES {
    home = '',
    currencies = 'currencies',
    wallet = 'wallet',
    notFound = '404'
}

export enum _ENDPOINTS {
    ticker = 'ticker/',
    listings = 'listings/'
}

export enum _STRINGS {
    appName = 'CriptX',
    homepageTitle = 'Trade Cryptocurrencies',
    appTagline = 'at the lowest commission in a secure environment',
    marketCapLabel = 'Market Cap:',
    priceLabel = 'Price:'
}
