import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderBy'
})

export class OrderByPipe implements PipeTransform {
    transform(array: any[], field: any, asc: boolean): any[] {
        if (!field || field.trim() === '') {
            return array;
        }

        // Ascending
        if (asc) {
            return Array.from(array).sort((a: any, b: any) => {
                return this.orderByComparator(a[field], b[field]);
            });
        } else {
            // Descending
            return Array.from(array).sort((a: any, b: any) => {
                return this.orderByComparator(b[field], a[field]);
            });
        }

        return;
    }

    private orderByComparator(a: any, b: any) {
        if (!a || !b) { return; }
        if ((isNaN(parseFloat(a))) || (!isFinite(a)) || (isNaN(parseFloat(b))) || !isFinite(b)) {
            // Isn't a number so lowercase the string to properly compare
            if (a.toLowerCase() < b.toLowerCase()) { return -1; }
            if (a.toLowerCase() > b.toLowerCase()) { return 1; }
        } else {
            // Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b)) { return -1; }
            if (parseFloat(a) > parseFloat(b)) { return 1; }
        }

        return 0;
    }
}
