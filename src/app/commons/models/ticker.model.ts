import { Deserialize } from 'src/app/commons/models/deserialize.model';

export interface IQuote {
    price: number;
    volume_24h: number;
    market_cap: number;
    percent_change_1h: number;
    percent_change_24h: number;
    percent_change_7d: number;
}

export type ISort = 'id' | 'rank' | 'volume_24h' | 'percent_change_24h';
export type IStructure = 'dictionary' | 'array';
export type IConvert = 'AUD' | 'BRL' | 'CAD' | 'CHF' | 'CLP' | 'CNY' | 'CZK' | 'DKK' | 'EUR' | 'GBP' | 'HKD' | 'HUF' | 'IDR' |
    'ILS' | 'INR' | 'JPY' | 'KRW' | 'MXN' | 'MYR' | 'NOK' | 'NZD' | 'PHP' | 'PKR' | 'PLN' | 'RUB' | 'SEK' | 'SGD' | 'THB' |
    'TRY' | 'TWD' | 'ZAR' | 'BTC' | 'ETH' | 'XRP' | 'LTC' | 'BCH';

export interface IQuotes {
    [key: string]: IQuote;
}

export interface IMetadata {
    error: any;
    num_cryptocurrencies: number;
    timestamp: number;
}

export class ITicker extends Deserialize {
    private cijena;
    id: number;
    name: string;
    symbol: string;
    website_slug: string;
    rank: number;
    circulating_supply: number;
    total_supply: number;
    max_supply: number;
    private quotes: IQuotes;


    constructor(data) {
        super();
        this.deserialize(data);
        this.cijena = Math.random().toFixed(2);
    }

    get price(): number {
        return parseFloat(this.quotes.USD.price.toFixed(2));
        // return this.cijena;
    }

    get marketCap(): number {
        return this.quotes.USD.market_cap;
    }

    get percentChange1h(): number {
        return this.quotes.USD.percent_change_1h;
    }

    get percentChange24h(): number {
        return this.quotes.USD.percent_change_24h;
    }

    get percentChange7d(): number {
        return this.quotes.USD.percent_change_7d;
    }

    get volume24h(): number {
        return this.quotes.USD.volume_24h;
    }

    get currency(): string {
        return Object.keys(this.quotes)[0];
    }
}
