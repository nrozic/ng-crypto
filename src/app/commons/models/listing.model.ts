import { Deserialize } from 'src/app/commons/models/deserialize.model';
import { IMetadata } from 'src/app/commons/models/ticker.model';
import { ICurrency } from 'src/app/commons/models/currency.model';

export class IListing extends Deserialize {
    id: number;
    name: string;
    symbol: string;
    website_slug: string;

    constructor(data) {
        super();
        this.deserialize(data);
    }
}
