import { Deserialize } from 'src/app/commons/models/deserialize.model';

export class ICurrency extends Deserialize {
    id: number;
    name: string;
    symbol: string;
    website_slug: string;

    constructor(data) {
        super();
        this.deserialize(data);
    }
}
