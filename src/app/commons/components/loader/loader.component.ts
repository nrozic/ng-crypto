import { Component, OnInit, AfterContentChecked, OnDestroy } from '@angular/core';

import { LoaderService } from 'src/app/commons/services/loader.service';
import { Subscription, Subscriber } from 'rxjs';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, AfterContentChecked, OnDestroy {
    private subscriptions = new Subscription();
    loaderVisible = true;

    constructor(
        private loaderService: LoaderService
    ) { }

    ngOnInit() {
    }

    ngAfterContentChecked() {
        this.shouldDisplayLoadingSpinner();
    }

    shouldDisplayLoadingSpinner() {
        const sub = this.loaderService.spinnerChange.subscribe((data) => {
            this.loaderVisible = data;
        });

        this.subscriptions.add(sub);
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }

}
