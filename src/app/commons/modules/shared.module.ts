import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { PreloadImageDirective } from 'src/app/commons/directives/preload-image.directive';
import { FilterPipe } from 'src/app/commons/pipes/filter.pipe';
import { LoaderModule } from 'src/app/commons/components/loader/loader.module';
import { HttpRequestsInterceptor } from 'src/app/commons/interceptors/http.request.interceptor';
import { ErrorsHandler } from 'src/app/commons/error-handlers/errors-handler';
import { CurrencyListComponent } from 'src/app/view_fragments/currency-list/currency-list.component';
import { SidebarComponent } from 'src/app/view_fragments/sidebar/sidebar.component';
import { OrderByPipe } from 'src/app/commons/pipes/order-by.pipe';
import { TransactionModalComponent } from 'src/app/view_fragments/currency-list/transaction-modal/transaction-modal.component';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        LoaderModule,
    ],
    declarations: [
        PreloadImageDirective,
        FilterPipe,
        CurrencyListComponent,
        SidebarComponent,
        OrderByPipe,
        TransactionModalComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpRequestsInterceptor,
            multi: true
        },
        {
            provide: ErrorHandler,
            useClass: ErrorsHandler,
        },
    ],
    exports: [
        FormsModule,
        LoaderModule,
        CurrencyListComponent,
        SidebarComponent,
        TransactionModalComponent
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                PreloadImageDirective,
                FilterPipe,
                OrderByPipe
            ]
        };
    }
}
