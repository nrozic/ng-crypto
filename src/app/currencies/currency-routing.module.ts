import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { _ROUTES } from '../commons/constants/app-constants';
import { CurrenciesComponent } from './currencies.component';

const currencyRoutes = [
    {
        path: '', component: CurrenciesComponent, children: [

        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(currencyRoutes)
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})
export class CurrencyRoutingModule { }
