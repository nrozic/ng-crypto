import { Injectable } from '@angular/core';
import { CurrenciesModule } from 'src/app/currencies/currencies.module';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ISort, ITicker } from 'src/app/commons/models/ticker.model';
import { HelperService } from 'src/app/commons/services/helper.service';
import { _ENDPOINTS } from 'src/app/commons/constants/app-constants';
import { HttpService } from 'src/app/commons/services/http.service';
import { IListing } from '../commons/models/listing.model';

@Injectable({
    providedIn: CurrenciesModule
})
export class CurrencyService {

    constructor(
        private httpService: HttpService
    ) { }

    getAllCurrencies(start: number, limit: number, sort: ISort): Observable<ITicker[]> {
        const url = HelperService.concatenateString(
            [_ENDPOINTS.ticker, `?&start=${start}&limit=${limit}&sort=${sort}&structure=array`], ''
        );
        return this.httpService.get<ITicker[]>(url).pipe(
            map(response => {
                return response.data.map(item => {
                    return new ITicker(item);
                });
            })
        );
    }

    getListings(): Observable<IListing[]> {

        return this.httpService.get<IListing[]>(_ENDPOINTS.listings).pipe(
            map(response => {
                return response.data.map(item => {
                    return new IListing(item);
                });
            })
        );
    }
}
