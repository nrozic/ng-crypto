import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { CurrencyService } from 'src/app/currencies/currency.service';
import { ITicker, ISort } from 'src/app/commons/models/ticker.model';
import { IListing } from 'src/app/commons/models/listing.model';

const _ITEMS_PER_PAGE = 20;

@Component({
    selector: 'app-currencies',
    templateUrl: './currencies.component.html',
    styleUrls: ['./currencies.component.scss']
})

export class CurrenciesComponent implements OnInit, OnDestroy {
    list: ITicker[] = [];
    endReached = false;
    private listings: IListing[];
    private subscriptions: Subscription = new Subscription();
    private start = 1;
    private sort: ISort = 'id';

    constructor(
        private currencyService: CurrencyService
    ) { }

    ngOnInit() {
        this.getListings();
        this.getAllCurrencies();
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }

    loadMore() {
        if (this.start >= this.listings.length) {
            this.endReached = true;
            return;
        }
        this.start = this.start;
        this.getAllCurrencies();
    }

    private getListings() {
        this.currencyService.getListings().subscribe(
            response => {
                this.listings = response;
            }
        );
    }

    private getAllCurrencies(): void {
        const subscription = this.currencyService.getAllCurrencies(this.start, _ITEMS_PER_PAGE, this.sort).subscribe(
            response => {
                // if (response.length > 0) { return; }
                this.list = this.list.concat(response);
                this.start = this.start + _ITEMS_PER_PAGE;
            },
            rsp => {
                console.error('There was an error', rsp);
            }
        );

        this.subscriptions.add(subscription);
    }

}
