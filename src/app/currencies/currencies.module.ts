import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrenciesComponent } from 'src/app/currencies/currencies.component';
import { SharedModule } from 'src/app/commons/modules/shared.module';
import { CurrencyRoutingModule } from 'src/app/currencies/currency-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        CurrencyRoutingModule
    ],
    declarations: [
        CurrenciesComponent
    ],
    exports: [
        CurrenciesComponent
    ]
})
export class CurrenciesModule { }
