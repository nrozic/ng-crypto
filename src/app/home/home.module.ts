import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/commons/modules/shared.module';
import { HomeRoutingModule } from 'src/app/home/home-routing.module';
import { HomeComponent } from 'src/app/home/home.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        HomeRoutingModule,
    ],
    declarations: [
        HomeComponent
    ],
    exports: [
        HomeComponent,
    ]
})
export class HomeModule { }
