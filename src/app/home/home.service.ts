import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpService } from 'src/app/commons/services/http.service';
import { _ENDPOINTS } from 'src/app/commons/constants/app-constants';
import { ITicker, ISort } from 'src/app/commons/models/ticker.model';
import { HelperService } from 'src/app/commons/services/helper.service';


@Injectable({
    providedIn: 'root'
})
export class HomeService {

    constructor(
        private httpService: HttpService
    ) { }

    getTopCurrencies(start: number, limit: number, sort: ISort): Observable<ITicker[]> {
        const url = HelperService.concatenateString([_ENDPOINTS.ticker, `?limit=${limit}&sort=${sort}&structure=array`], '');
        console.log('URL JE: ', url);
        return this.httpService.get<ITicker[]>(url).pipe(
            map(response => {
                return response.data.map(item => {
                    return new ITicker(item);
                });
            })
        );
    }
}
