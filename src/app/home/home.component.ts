import { Component, OnInit, OnDestroy } from '@angular/core';
import { HomeService } from 'src/app/home/home.service';
import { ITicker } from '../commons/models/ticker.model';
import { _CONFIG, _ROUTES } from '../commons/constants/app-constants';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [
        HomeService
    ]
})
export class HomeComponent implements OnInit, OnDestroy {
    list: ITicker[];
    allCurrenciesRoute: String = _ROUTES.currencies;
    allCurrenciesText = 'All currencies';
    private subscriptions: Subscription = new Subscription();
    private heartbeatInterval: any;

    constructor(
        private homeService: HomeService,
    ) { }

    ngOnInit() {
        this.getTopCurrencies();
        this.heartbeat();
    }

    ngOnDestroy() {
        clearInterval(this.heartbeatInterval);
        this.subscriptions.unsubscribe();
    }

    private getTopCurrencies(): void {
        const subscription = this.homeService.getTopCurrencies(1, 10, 'rank').subscribe(
            response => {
                this.list = response;
            }
        );

        this.subscriptions.add(subscription);
    }

    private heartbeat(): void {
        this.heartbeatInterval = setInterval(() => {
            this.getTopCurrencies();
        }, _CONFIG.updateFrequency);
    }

}
