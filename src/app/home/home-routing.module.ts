import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { _ROUTES } from 'src/app/commons/constants/app-constants';
import { HomeComponent } from 'src/app/home/home.component';


const homeRoutes: Routes = [
    { path: _ROUTES.home, component: HomeComponent },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(homeRoutes)
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})
export class HomeRoutingModule { }
